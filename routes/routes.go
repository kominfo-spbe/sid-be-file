package routes

import (
	"os"
	"sidat-file/controllers"

	"github.com/gofiber/fiber/v2"
	jwtware "github.com/gofiber/jwt/v3"
)

func Setup(app *fiber.App) {

	// must be logged-in
	//app.Use(middleware.IsAuthenticated)
	app.Use(jwtware.New(jwtware.Config{
		SigningKey: []byte(os.Getenv("TOKEN_SECRET")),
	}))

	app.Get("/api/file/info", controllers.FileInfo)
	app.Post("/api/file/upload", controllers.FileUploadSingle)
	app.Post("/api/file/multi-upload", controllers.FileUploadMulti)
	app.Delete("/api/file/delete", controllers.FileDelete)
}
