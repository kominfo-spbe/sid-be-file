package controllers

import (
	"fmt"
	"os"
	"sidat-file/models/reqresp"
	"sidat-file/util"
	"strconv"

	"github.com/gofiber/fiber/v2"
)

type fileInfo struct {
	Name         string `json:"name"`
	Size         int    `json:"size"`
	Permissions  string `json:"permissions"`
	LastModified string `json:"last_modified"`
	IsDirectory  bool   `json:"is_directory"`
}

// FileInfo Get info of uploaded file
// @Summary Get info of uploaded file
// @Description Get info of uploaded file
// @Tags File
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param path query string true "File path"
// @Success 200 {object} fileInfo
// @Router /file/info [get]
func FileInfo(c *fiber.Ctx) error {
	fullpath := c.Query("path", "/")

	fStats, err := os.Stat("./public/" + fullpath)
	if err != nil {
		return c.Status(404).JSON(err)
	}

	return c.JSON(&fileInfo{
		fStats.Name(),
		int(fStats.Size()),
		strconv.Itoa(int(fStats.Mode())),
		fStats.ModTime().String(),
		fStats.IsDir(),
	})
}

// FileDelete Delete uploaded file
// @Summary Delete uploaded file
// @Description Delete uploaded file
// @Tags File
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param path query string true "File path"
// @Success 200 {object} reqresp.SuccessResponse
// @Router /file/delete [delete]
func FileDelete(c *fiber.Ctx) error {
	fullpath := c.Query("path", "/")

	_, err := os.Stat("./public/" + fullpath)
	if err != nil {
		return c.Status(404).JSON(err)
	}

	// delete uploaded file
	_ = os.Remove("./public/" + fullpath)
	//if err != nil {
	//	return c.Status(fiber.StatusInternalServerError).JSON(err)
	//}

	// send to log
	util.SendToAudit(c, "file", "delete", nil, &reqresp.SuccessResponse{
		Status:  "success",
		Message: "File deleted",
		Data:    fullpath,
	})

	c.Status(fiber.StatusOK)
	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "File deleted",
		Data:    fullpath,
	})
}

type fileUploadRes struct {
	Path string `json:"path"`
}

// FileUploadSingle Upload single file
// @Summary Upload single file
// @Description Upload single file
// @Tags File
// @Accept mpfd
// @Produce json
// @param Authorization header string true "Authorization"
// @param file formData file true "The file"
// @Success 200 {object} fileUploadRes
// @Router /file/upload [post]
func FileUploadSingle(c *fiber.Ctx) error {
	file, err := c.FormFile("file")

	_, e := os.Stat("./public/uploads")
	if os.IsNotExist(e) {
		err := os.MkdirAll("./public/uploads", 0755)
		if err != nil {
			return err
		}
	}

	if err != nil {
		return err
	}

	newFilename := util.RandString(8) + file.Filename
	//Save file inside uploads folder under current working directory:
	err = c.SaveFile(file, fmt.Sprintf("./public/uploads/%s", newFilename))
	if err != nil {
		return err
	}

	// send to log
	util.SendToAudit(c, "file", "upload", nil, &fileUploadRes{
		Path: "uploads/" + newFilename,
	})

	return c.JSON(&fileUploadRes{
		Path: "uploads/" + newFilename,
	})
}

type multiFileUploadRes struct {
	Message string   `json:"message"`
	Data    []string `json:"data"`
}

// FileUploadMulti FileUploadSingle Upload multiple file
// @Summary Upload multiple file
// @Description Upload multiple file
// @Tags File
// @Accept mpfd
// @Produce json
// @param Authorization header string true "Authorization"
// @param files formData file true "The file"
// @Success 200 {object} fileUploadRes
// @Router /file/multi-upload [post]
func FileUploadMulti(c *fiber.Ctx) error {
	// Parse the multipart form:
	form, err := c.MultipartForm()
	if err != nil {
		return err
	}
	// => *multipart.Form

	_, e := os.Stat("./public/uploads")
	if os.IsNotExist(e) {
		err := os.MkdirAll("./public/uploads", 0755)
		if err != nil {
			return err
		}
	}

	// Get all files from "documents" key:
	files := form.File["files"]
	// => []*multipart.FileHeader

	newFileNames := make([]string, len(files))

	// Loop through files:
	for dx, file := range files {
		fmt.Println(file.Filename, file.Size, file.Header["Content-Type"][0])
		// => "tutorial.pdf" 360641 "application/pdf"

		newFilename := util.RandString(8) + file.Filename

		// Save the files to disk:
		err := c.SaveFile(file, fmt.Sprintf("./public/uploads/%s", newFilename))

		// Check for errors
		if err != nil {
			return err
		}

		newFileNames[dx] = "uploads/" + newFilename
	}

	// send to log
	util.SendToAudit(c, "file", "multi-upload", nil, &multiFileUploadRes{
		Message: "success",
		Data:    newFileNames,
	})

	return c.JSON(&multiFileUploadRes{
		Message: "success",
		Data:    newFileNames,
	})
}
