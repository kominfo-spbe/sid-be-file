package models

import (
	"time"
)

type Role struct {
	Id         uint           `json:"id" gorm:"primaryKey"`
	CreatedAt  time.Time      `json:"created_at"`
	UpdatedAt  time.Time      `json:"updated_at"`
	Name       string         `json:"name" gorm:"unique"`
	Permission []Permission   `json:"permissions" gorm:"many2many:role_permissions"`
}

func (r Role) TableName() string {
	return "user_mgt.role"
}
