module sidat-file

go 1.16

require (
	github.com/andybalholm/brotli v1.0.3 // indirect
	github.com/arsmn/fiber-swagger/v2 v2.17.0
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-openapi/jsonreference v0.19.6 // indirect
	github.com/go-openapi/swag v0.19.15 // indirect
	github.com/gofiber/fiber/v2 v2.18.0
	github.com/gofiber/jwt/v3 v3.0.2
	github.com/golang-jwt/jwt/v4 v4.0.0
	github.com/jackc/pgtype v1.8.1
	github.com/jackc/pgx/v4 v4.13.0 // indirect
	github.com/joho/godotenv v1.3.0
	github.com/klauspost/compress v1.13.5 // indirect
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/swaggo/files v0.0.0-20210815190702-a29dd2bc99b2 // indirect
	github.com/swaggo/swag v1.7.1
	golang.org/x/crypto v0.0.0-20210817164053-32db794688a5
	golang.org/x/net v0.0.0-20210907225631-ff17edfbf26d // indirect
	golang.org/x/sys v0.0.0-20210906170528-6f6e22806c34 // indirect
	golang.org/x/text v0.3.7 // indirect
	golang.org/x/tools v0.1.5 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
	gorm.io/driver/postgres v1.1.0
	gorm.io/gorm v1.21.14
)
